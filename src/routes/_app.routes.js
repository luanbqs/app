import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import Restaurants from '../pages/Restaurants';
import SearchRestaurant from '../pages/SearchRestaurant';
import RestaurantDetails from '../pages/RestaurantDetails';
import { appRoutes } from '../utils/constants';

const App = createStackNavigator();

const AppRoutes = () => (
  <>
    <App.Navigator
      screenOptions={{
        headerShown: false,
      }}
    >
      <App.Screen name={appRoutes.RESTAURANTS} component={Restaurants} />
      <App.Screen
        name={appRoutes.SEARCH_RESTAURANTS}
        component={SearchRestaurant}
      />
      <App.Screen
        name={appRoutes.RESTAURANTS_DETAILS}
        component={RestaurantDetails}
      />
    </App.Navigator>
  </>
);

export default AppRoutes;
