/* eslint-disable no-nested-ternary */
import React from 'react';
import AuthRoutes from './_app.routes';

function Routes() {
  return <AuthRoutes />;
}

export default Routes;
