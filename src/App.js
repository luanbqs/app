import 'react-native-gesture-handler';

import React from 'react';
import { Provider } from 'react-redux';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import './config/ReactotronConfig';

import { store } from './store';
import Routes from './routes';

const Src = () => (
  <NavigationContainer>
    <Provider store={store}>
      <StatusBar barStyle="dark-content" translucent />
      <Routes />
    </Provider>
  </NavigationContainer>
);

export default Src;
