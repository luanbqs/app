/* eslint-disable no-param-reassign */
/* eslint-disable array-callback-return */
import { all, takeEvery, call, put } from 'redux-saga/effects';

import api from '../../../services/api';
import { apiPaths } from '../../../utils/constants';
import {
  actions,
  getRestaurantsSuccess,
  getRestaurantsFailed,
} from './actions';

export function* getRestaurants({ payload }) {
  try {
    const response = yield call(api.get, apiPaths.GET_RESTAURANTS, {
      params: {
        page: payload.page,
        limit: 10,
        search: payload.search,
      },
    });
    response.data.data.map((item) => {
      item.logo = `${item.logo}&random=${Math.random()
        .toString(36)
        .substring(7)}`;
      item.image = `${item.image}&random=${Math.random()
        .toString(36)
        .substring(7)}`;
    });

    // eslint-disable-next-line no-unused-expressions
    response.data.data.length % 2 !== 0
      ? response.data.data.push([{ name: 'invisible', logo: '', image: '' }])
      : null;
    yield put(
      getRestaurantsSuccess({
        restaurants: response.data.data,
        total: response.data.pagination.total,
        perPage: response.data.pagination.per_page,
        shouldRefresh: payload.shouldRefresh,
      })
    );
  } catch (err) {
    yield put(getRestaurantsFailed());
  }
}

export default all([takeEvery(actions.ASYNC_GET_RESTAURANTES, getRestaurants)]);
