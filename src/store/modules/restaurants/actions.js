export const actions = {
  ASYNC_GET_RESTAURANTES: '@restaurants:ASYNC_GET_RESTAURANTES',
  GET_RESTAURANTES_SUCCESS: '@restaurants:GET_RESTAURANTES_SUCCESS',
  GET_NEXT_PAGE_SUCCESS: '@restaurants:GET_NEXT_PAGE_SUCCESS',
  GET_RESTAURANTES_FAILED: '@restaurants:GET_RESTAURANTES_FAILED',
};

export const asyncGetRestaurants = (payload) => ({
  type: actions.ASYNC_GET_RESTAURANTES,
  payload,
});

export const getRestaurantsSuccess = (payload) => ({
  type: actions.GET_RESTAURANTES_SUCCESS,
  payload,
});

export const getRestaurantsFailed = (payload) => ({
  type: actions.GET_RESTAURANTES_FAILED,
  payload,
});
