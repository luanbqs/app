import { actions } from './actions';

const INITIAL_STATE = {
  restaurantsList: [],
  total: 0,
  perPage: 10,
  loading: false,
  error: '',
};

export default function restaurants(state = INITIAL_STATE, { type, payload }) {
  switch (type) {
    case actions.ASYNC_GET_RESTAURANTES:
      return {
        ...state,
        loading: true,
      };

    case actions.GET_RESTAURANTES_SUCCESS:
      return {
        ...state,
        restaurantsList: payload.shouldRefresh
          ? payload.restaurants
          : [...state.restaurantsList, ...payload.restaurants],
        total: Math.floor(payload.total / payload.perPage),
        loading: false,
        error: '',
      };

    case actions.GET_RESTAURANTES_FAILED:
      return {
        ...state,
        loading: false,
        error: payload,
      };
    default:
      return state;
  }
}
