import { combineReducers } from 'redux';
import restaurants from './restaurants/reducer';


const AppReducer = combineReducers({
  restaurants,

});

export default AppReducer;
