import { all } from 'redux-saga/effects';

import restaurants from './restaurants/saga';

export default function* rootSaga() {
  return yield all([
    restaurants,
  ]);
}
