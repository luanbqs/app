/* eslint-disable no-undef */
import Reactotron from 'reactotron-react-native';
import { reactotronRedux } from 'reactotron-redux';
import reacotronSaga from 'reactotron-redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';

if (__DEV__) {
  const tron = Reactotron.setAsyncStorageHandler(AsyncStorage)
    .configure()
    .configure({ host: '192.168.1.3' }) // use in iOS device
    .useReactNative()
    .use(reactotronRedux())
    .use(reacotronSaga())
    .connect();

  tron.clear();

  console.tron = tron;
}
