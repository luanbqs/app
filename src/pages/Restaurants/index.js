/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import Input from '../../components/Input';
import Loading from '../../components/Loading';
import headerImage from '../../assets/images/header.png';
import searchIcon from '../../assets/images/search_red.png';
import { appRoutes } from '../../utils/constants';
import { asyncGetRestaurants } from '../../store/modules/restaurants/actions';
import {
  Container,
  HeaderImage,
  RestarantsList,
  Title,
  Description,
  ListHeader,
  ListTitle,
  ListWrapper,
  RestaurantCard,
  RestaurantName,
  CardWrapper,
} from './styles';

const Restaurants = () => {
  const dispatch = useDispatch();
  const navigation = useNavigation();

  const [page, setPage] = useState(1);
  const restaurants = useSelector((state) => state.restaurants.restaurantsList);
  const totalPages = useSelector((state) => state.restaurants.total);

  const restaurantsLoading = useSelector((state) => state.restaurants.loading);

  useEffect(() => {
    loadList();
  }, []);

  function loadList(pageNumber = page, shouldRefresh = false) {
    if (totalPages && pageNumber > totalPages) return;
    dispatch(
      asyncGetRestaurants({ page: pageNumber, shouldRefresh, search: '' })
    );
    setPage(pageNumber + 1);
  }

  function refreshList() {
    loadList(1, true);
  }

  const renderItem = ({ item }) => (
    <CardWrapper
      onPress={() =>
        navigation.navigate(appRoutes.RESTAURANTS_DETAILS, {
          item,
        })
      }
      key={item.name}
    >
      <RestaurantCard logo={item.image} key={item.name}>
        <RestaurantName>{item.name}</RestaurantName>
      </RestaurantCard>
    </CardWrapper>
  );

  function renderHeader() {
    return (
      <ListHeader>
        <Input
          onEndEditing={(e) => handleSearch(e.nativeEvent.text)}
          placeholder="Encontre um restaurante"
          icon={searchIcon}
        />
        <ListTitle>Restaurantes</ListTitle>
      </ListHeader>
    );
  }

  function handleSearch(text) {
    if (text) {
      navigation.navigate(appRoutes.SEARCH_RESTAURANTS, {
        search: text,
        shouldRefresh: true,
      });
    } else {
      refreshList();
    }
  }

  return (
    <Container>
      <HeaderImage source={headerImage}>
        <Title testID="restaurant-title">Descubra novos {'\n'}sabores</Title>
        <Description>
          Aqui eu converso com você sobre nossa proposta
        </Description>
      </HeaderImage>
      <ListWrapper>
        <RestarantsList
          keyExtractor={(item) => item.name}
          testID="restaurant-list"
          data={restaurants}
          onRefresh={() => refreshList()}
          refreshing={restaurantsLoading ?? false}
          numColumns={2}
          ListHeaderComponent={renderHeader}
          ListFooterComponent={restaurantsLoading && <Loading />}
          onEndReached={() => loadList()}
          renderItem={renderItem}
          onEndReachedThreshold={0.1}
        />
      </ListWrapper>
    </Container>
  );
};

export default Restaurants;
