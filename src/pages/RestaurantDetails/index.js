import React from 'react';
import Header from '../../components/Header';
import {
  Container,
  HeaderImage,
  DetailsContainer,
  RestaurantName,
  RestaurantLogo,
  TitleWrapper,
  Section,
  DataWrapper,
  DataTitle,
  DataText,
  Separetor,
  LogoWrapper,
} from './styles';

const RestaurantDetails = ({ route }) => {
  const { item } = route.params;
  return (
    <Container>
      <HeaderImage source={{ uri: item.image }}>
        <Header iconType="white" />
      </HeaderImage>
      <DetailsContainer>
        <TitleWrapper>
          <LogoWrapper>
            <RestaurantLogo source={{ uri: item.logo }} />
          </LogoWrapper>
          <RestaurantName>{item.name}</RestaurantName>
        </TitleWrapper>
        <DataWrapper>
          <Section>
            <DataTitle>Descrição</DataTitle>
            <DataText>{item.description}</DataText>
          </Section>
          <Section>
            <DataTitle>Contato</DataTitle>
            <DataText>{item.telephone}</DataText>
            <DataText>{item.website}</DataText>
          </Section>
          <Section>
            <DataTitle>Faixa de Preço</DataTitle>
            <DataText>{item.price_range}</DataText>
          </Section>
          <Separetor />
          <Section>
            <DataTitle>Horários de Funcionamento</DataTitle>
            <DataText>{item.opening_hours}</DataText>
          </Section>
          <Section>
            <DataTitle>Formas de pagamento</DataTitle>
            <DataText>{item.payment_methods}</DataText>
          </Section>
        </DataWrapper>
      </DetailsContainer>
    </Container>
  );
};

export default RestaurantDetails;
