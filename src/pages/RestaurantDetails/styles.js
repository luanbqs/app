import styled from 'styled-components/native';

export const Container = styled.View`
  flex: 1;
  background-color: #ffffff;
`;

export const HeaderImage = styled.ImageBackground.attrs(() => ({
  imageStyle: {
    borderRadius: 8,
    opacity: 0.5,
    height: 200,
    width: '100%',
  },
}))`
  height: 200px;
  background-color: #000000;
  width: 100%;
`;

export const DetailsContainer = styled.View`
  background-color: #ffffff;
  margin-top: -10%;
  flex: 1;
  border-top-left-radius: 32px;
  border-top-right-radius: 32px;
`;

export const LogoWrapper = styled.View`
  width: 121px;
  height: 121px;
  justify-content: center;
  align-items: center;
  background-color: #ffffff;
  border-radius: 60px;
`;

export const RestaurantLogo = styled.Image`
  width: 119px;
  height: 119px;
  border-radius: 60px;
`;

export const TitleWrapper = styled.View`
  width: 100%;
  justify-content: center;
  align-items: center;
  margin-top: -60px;
`;

export const RestaurantName = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 20px;
  text-align: center;
  margin-top: 15px;
  color: #333333;
`;

export const DataWrapper = styled.ScrollView`
  padding: 0 31px;
  margin-bottom: 20px;
`;

export const Section = styled.View``;

export const DataTitle = styled.Text`
  margin-top: 30px;
  font-family: 'Poppins-SemiBold';
  font-size: 15px;
  text-align: left;
  color: #333333;
`;

export const DataText = styled.Text`
  margin-top: 1px;
  font-family: 'Poppins-Regular';
  font-size: 14px;
  text-align: left;
  color: #808080;
`;

export const Separetor = styled.View`
  margin-top: 30px;
  width: 100%;
  height: 1px;
  background-color: #cccccc;
`;
