import styled from 'styled-components/native';
import {
  getBottomSpace,
  getStatusBarHeight,
} from 'react-native-iphone-x-helper';

export const Container = styled.View`
  flex: 1;
  padding-top: ${getBottomSpace()}px;
  background: #ffffff;
`;

export const ListWrapper = styled.View`
  flex: 7;
  background: #fff;
  margin-top: 100px;
  padding: 10px;
  padding-bottom: ${getStatusBarHeight()}px;

  border-top-left-radius: 32px;
  border-top-right-radius: 32px;
`;

export const ListHeader = styled.View`
  padding: 20px;
`;

export const ListTitle = styled.Text`
  margin-top: 25px;
  font-size: 18px;
  font-style: normal;
  font-weight: 700;
  line-height: 21px;
  text-align: left;
  justify-content: space-around;
`;

export const RestarantsList = styled.FlatList.attrs({
  contentContainerStyle: {
    alignItems: 'center',
  },
})`
  flex: 1;
`;

export const CardWrapper = styled.TouchableOpacity`
  height: 170px;
  width: 158px;
  margin: 15px;
  border-radius: 8px;
`;

export const RestaurantCard = styled.ImageBackground.attrs((props) => ({
  imageStyle: {
    borderRadius: 8,
    opacity: 0.5,
  },
  source: {
    uri: props.logo,
    cache: 'force-cache',
  },
}))`
  position: relative;
  height: 170px;
  width: 158px;
  border-radius: 8px;
  background-color: ${(props) => (props.logo ? '#000' : '#fff')};
`;

export const RestaurantName = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 15px;
  font-style: normal;
  line-height: 18px;
  text-align: left;
  color: #fff;
  flex-direction: row;
  position: absolute;
  bottom: 10px;
  left: 10px;
`;
