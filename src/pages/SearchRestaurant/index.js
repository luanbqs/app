/* eslint-disable no-shadow */
/* eslint-disable no-unused-expressions */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { useNavigation } from '@react-navigation/native';
import Header from '../../components/Header';
import Input from '../../components/Input';
import Loading from '../../components/Loading';

import searchIcon from '../../assets/images/search_red.png';
import { asyncGetRestaurants } from '../../store/modules/restaurants/actions';
import { appRoutes } from '../../utils/constants';

import {
  Container,
  ListWrapper,
  ListHeader,
  ListTitle,
  RestarantsList,
  CardWrapper,
  RestaurantCard,
  RestaurantName,
} from './styles';

const SearchRestaurant = ({ route }) => {
  const dispatch = useDispatch();
  const { search, shouldRefresh } = route.params;
  const navigation = useNavigation();

  const [page, setPage] = useState(1);
  const [searchParam, setSearchParam] = useState(search);

  const restaurants = useSelector((state) => state.restaurants.restaurantsList);
  const totalPages = useSelector((state) => state.restaurants.total);

  const restaurantsLoading = useSelector((state) => state.restaurants.loading);

  useEffect(() => {
    loadList(1, shouldRefresh, search);
  }, []);

  function loadList(
    pageNumber = page,
    shouldRefresh = false,
    search = searchParam
  ) {
    if (totalPages && pageNumber > totalPages) return;
    dispatch(
      asyncGetRestaurants({
        page: pageNumber,
        shouldRefresh,
        search,
      })
    );
    setPage(pageNumber + 1);
  }

  function refreshList() {
    // ToDo
    // Scroll to top on refresh list
    loadList(1, true);
  }

  const renderItem = ({ item }) => (
    <CardWrapper
      onPress={() =>
        navigation.navigate(appRoutes.RESTAURANTS_DETAILS, {
          item,
        })
      }
      key={item.name}
    >
      <RestaurantCard logo={item.image} key={item.name}>
        <RestaurantName>{item.name}</RestaurantName>
      </RestaurantCard>
    </CardWrapper>
  );

  return (
    <Container>
      <Header title="Resultado para" search={search} />
      <ListWrapper>
        <ListHeader>
          <Input
            onEndEditing={() => refreshList()}
            onChangeText={(text) => setSearchParam(text)}
            placeholder="Encontre um restaurante"
            icon={searchIcon}
            defaultValue={search}
          />
          <ListTitle>Restaurantes</ListTitle>
        </ListHeader>
        <RestarantsList
          keyExtractor={(item) => item.name}
          testID="search-restaurant-list"
          data={restaurants}
          onRefresh={() => refreshList()}
          refreshing={restaurantsLoading ?? false}
          numColumns={2}
          ListFooterComponent={restaurantsLoading && <Loading />}
          onEndReached={() => loadList()}
          renderItem={renderItem}
          onEndReachedThreshold={0.1}
        />
      </ListWrapper>
    </Container>
  );
};

export default SearchRestaurant;
