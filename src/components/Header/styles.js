import styled from 'styled-components/native';
import { Platform } from 'react-native';
import { getBottomSpace } from 'react-native-iphone-x-helper';

export const Container = styled.View`
  position: absolute;
  top: ${Platform.OS === 'ios' ? `${getBottomSpace() + 30}px` : '50px'};
  left: 0;
  right: 0;
  flex-direction: row;
  align-items: center;
  justify-content: space-around;
  height: 55px;
`;

export const TitleContainer = styled.View`
  justify-content: center;
  align-items: center;
`;

export const Title = styled.Text`
  font-family: 'Poppins-Regular';
  font-size: 15px;
  color: #aaaaaa;
  min-width: 100px;
`;

export const SearchText = styled.Text`
  font-family: 'Poppins-SemiBold';
  font-size: 20px;
  color: #000;
`;

export const IconWrapper = styled.TouchableOpacity`
  justify-content: center;
  align-items: center;
  width: 55px;
  height: 55px;
  background-color: transparent;
  border-radius: 30px;
`;
