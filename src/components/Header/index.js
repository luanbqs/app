import React from 'react';
import { View, Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import chevronLeft from '../../assets/images/chevron_left_black.png';
import chevronLeftWhite from '../../assets/images/chevron_left_white.png';

import {
  Container,
  Title,
  IconWrapper,
  SearchText,
  TitleContainer,
} from './styles';

const FormHeader = ({ title, search, iconType }) => {
  const navigation = useNavigation();
  return (
    <Container>
      <IconWrapper onPress={() => navigation.goBack()}>
        <Image source={iconType === 'white' ? chevronLeftWhite : chevronLeft} />
      </IconWrapper>

      <TitleContainer>
        <Title>{title || ''}</Title>
        <SearchText>{search || ''}</SearchText>
      </TitleContainer>
      <View style={{ width: 55 }} />
    </Container>
  );
};

export default FormHeader;
