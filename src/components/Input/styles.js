import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding: 5px 30px;
  height: 60px;
  border-radius: 8px;
  border-width: 1px;
  border-color: #e6e6e6;
`;

export const SearchInput = styled.TextInput`
  height: 100%;
  width: 100%;
  font-size: 18px;
`;

export const InputIcon = styled.Image`
  margin-right: 10px;
`;
