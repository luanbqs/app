/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import { Container, SearchInput, InputIcon } from './styles';

const Input = ({ icon, placeholder = '', onFocus = () => {}, ...rest }) => {
  return (
    <Container>
      {icon && <InputIcon source={icon} />}
      <SearchInput
        onFocus={onFocus}
        placeholderTextColor="#666666"
        placeholder={placeholder}
        {...rest}
      />
    </Container>
  );
};

export default Input;
