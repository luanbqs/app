import React, { useEffect } from 'react';

import { Animated, Easing } from 'react-native';
import loadingIcon from '../../assets/images/loading_red.png';
import { Container, LoadingText } from './styles';

const Loading = ({ loading = false }) => {
  const rotateValue = new Animated.Value(0);

  const rotateData = rotateValue.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '360deg'],
  });

  useEffect(() => {
    if (loading) {
      startAnimation();
    }
  }, [loading]);

  function startAnimation() {
    Animated.loop(
      Animated.timing(rotateValue, {
        toValue: 1,
        duration: 3000,
        easing: Easing.linear,
        useNativeDriver: true,
      })
    ).start();
  }
  return (
    <Container>
      <LoadingText>Carregando</LoadingText>
      <Animated.Image
        style={{ transform: [{ rotate: rotateData }] }}
        source={loadingIcon}
      />
    </Container>
  );
};

export default Loading;
