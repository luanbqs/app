import styled from 'styled-components/native';

export const Container = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;

export const LoadingText = styled.Text`
  font-family: 'Poppins-Regular';
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  text-align: left;
  color: #808080;
  margin-right: 5px;
`;
