/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react-native';

import * as reactRedux from 'react-redux';
import * as reactNavigation from '@react-navigation/native';

import Restaurants from '../../src/pages/Restaurants';

jest.mock('react-redux');
jest.mock('@react-navigation/native');

describe('Restaurants', () => {
  it('should call dispatch', () => {
    jest.spyOn(reactRedux, 'useSelector');
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
    const useNavigationMock = jest.spyOn(reactNavigation, 'useNavigation');
    const dispatch = jest.fn();
    const navigation = jest.fn();
    useDispatchMock.mockReturnValue(dispatch);
    useNavigationMock.mockReturnValue(navigation);

    render(<Restaurants />);
    expect(dispatch).toHaveBeenCalledWith({
      type: '@restaurants:ASYNC_GET_RESTAURANTES',
      payload: { page: 1, search: '', shouldRefresh: false },
    });
  });

  it('should render restaurant list', () => {
    const useSelectorMock = jest.spyOn(reactRedux, 'useSelector');
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
    const dispatch = jest.fn();

    useDispatchMock.mockReturnValue(dispatch);
    useSelectorMock.mockReturnValue([
      { name: 'restaurante1' },
      { name: 'restaurante2' },
      { name: 'restaurante3' },
    ]);
    useSelectorMock.mockReturnValue(false);
    const { getByTestId, getByText } = render(<Restaurants />);

    expect(getByTestId('restaurant-list')).toContainElement(
      getByText('restaurante2')
    );

    expect(getByTestId('restaurant-list')).toContainElement(
      getByText('restaurante3')
    );
  });
});
