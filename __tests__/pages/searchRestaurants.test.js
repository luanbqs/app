/* eslint-disable no-undef */
import React from 'react';
import { render } from '@testing-library/react-native';

import * as reactRedux from 'react-redux';
import * as reactNavigation from '@react-navigation/native';

import SearchRestaurant from '../../src/pages/SearchRestaurant';

jest.mock('react-redux');
jest.mock('@react-navigation/native');

describe('SearchRestaurant', () => {
  it('should call dispatch', () => {
    jest.spyOn(reactRedux, 'useSelector');
    const useDispatchMock = jest.spyOn(reactRedux, 'useDispatch');
    const useNavigationMock = jest.spyOn(reactNavigation, 'useNavigation');
    const dispatch = jest.fn();
    const navigation = jest.fn();
    useDispatchMock.mockReturnValue(dispatch);
    useNavigationMock.mockReturnValue(navigation);

    render(<SearchRestaurant route={{ params: {} }} />);
    expect(dispatch).toHaveBeenCalledWith({
      type: '@restaurants:ASYNC_GET_RESTAURANTES',
      payload: { page: 1, search: undefined, shouldRefresh: false },
    });
  });
});
